<?php

function simple_msn_feed_rss_author_formatterview($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  // Default value for name.
  $name = _simple_msn_feed_variable_get('simple_msn_feed_publisher');

  // We consider only the first author.
  if (!empty($items[0])) {
    $item = $items[0];

    // If the formatter was applied to the textfield.
    if ($field['type'] == 'text') {
      $name = empty($item['value']) ? _simple_msn_feed_variable_get('simple_msn_feed_publisher') : $item['value'];
    }

    // If the formatter was applied to the node_reference field
    // and the reference is not empty.
    if ($field['type'] == 'node_reference' && !empty($item['nid'])) {
      if ($author_node = node_load($item['nid'])) {
        $name = empty($author_node->title) ? _simple_msn_feed_variable_get('simple_msn_feed_publisher') : $author_node->title;
      }
    }
  }

  $entity->rss_elements[] = array(
    'key' => 'author',
    'value' => $name,
  );

  return $element;
}
