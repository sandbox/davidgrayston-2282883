<?php

function simple_msn_feed_rss_media_formattersettings($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $options = array();
  $fields = field_read_fields(array('type' => 'text', 'entity_type' => 'file'));
  foreach ($fields as $f => $set) {
    $options[$f] = $f;
  }
  ksort($options);

  $element[] = array();
  $element['media_title'] = array(
    '#title' => t('media:title element'),
    '#type' => 'select',
    '#default_value' => $settings['media_title'],
    '#empty_option' => '',
    '#options' => $options,
  );
  $element['media_credit'] = array(
    '#title' => t('media:credit element'),
    '#type' => 'select',
    '#default_value' => $settings['media_credit'],
    '#empty_option' => '',
    '#options' => $options,
  );
  $element['media_credit_default'] = array(
    '#title' => t('media:credit default'),
    '#type' => 'textfield',
    '#default_value' => $settings['media_credit_default'],
  );
  return $element;
}

function simple_msn_feed_rss_media_formattersummary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = 'Please set <strong>media:title</strong> and <strong>media:credit</strong> parameters';

  // If title and credit parameters were set.
  if (!empty($settings['media_title']) && (!empty($settings['media_credit']) || !empty($settings['media_credit_default']))) {
    $summary = t('<strong>media:title</strong> is set to use the <strong>@title</strong> field and <strong>media:credit</strong> to <strong>@credit</strong>', array(
      '@title' => $settings['media_title'],
      '@credit' => empty($settings['media_credit']) ? '"' . $settings['media_credit_default'] . '"' : $settings['media_credit'] . ' field',
      )
    );
  }
  return $summary;
}

function simple_msn_feed_rss_media_formatterview($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  foreach ($items as $item) {
    if (empty($item['uri'])) {
      continue;
    }

    // Get <media:title>.
    if (!empty($display['settings']['media_title']) && !empty($item[$display['settings']['media_title']][LANGUAGE_NONE][0]['safe_value'])) {
      $title = $item[$display['settings']['media_title']][LANGUAGE_NONE][0]['safe_value'];
    }
    else {
      // If title is not detected, then load title of the entity
      // which is referencing that media file.
      $title = $entity->title;
    }

    // Getting data from the text field required for <media:credit>.
    if (!empty($display['settings']['media_credit']) && !empty($item[$display['settings']['media_credit']][LANGUAGE_NONE][0]['safe_value'])) {
      $credit = $item[$display['settings']['media_credit']][LANGUAGE_NONE][0]['safe_value'];
    }
    else {
      // Otherwise use field defaults, because this is a required field.
      $credit = $display['settings']['media_credit_default'];
    }

    // If $credit is still empty, use global defaults.
    $credit = empty($credit) ? _simple_msn_feed_variable_get('simple_msn_feed_credits') : $credit;

    // Constructing media:content element.
    $media_arr = array(
      'key' => 'media:content',
      'attributes' => array(),
    );

    if ($item['type'] == 'image') {
      // Applying simple_msn_feed style to the image.
      $msn_image_style = _simple_msn_feed_variable_get('simple_msn_feed_image_style');
      $styled_url = image_style_url($msn_image_style, $item['uri']);
      $styled_path = image_style_path($msn_image_style, $item['uri']);
      $info = image_get_info($styled_path);

      $media_arr['attributes']['url'] = $styled_url;
      if (!empty($info['file_size'])) {
        $media_arr['attributes']['length'] = $info['file_size'];
      }
      if (!empty($info['mime_type'])) {
        $media_arr['attributes']['type'] = $info['mime_type'];
      }
    }
    // In case if it is not an image.
    else {
      $media_arr['attributes']['url'] = file_create_url($item['uri']);
    }

    if (!empty($title)) {
      $media_arr['value']['media:title'] = $title;
    }

    if (!empty($credit)) {
      $media_arr['value']['media:credit'] = $credit;
    }

    $entity->rss_namespaces['xmlns:media'] = 'http://search.yahoo.com/mrss/';
    $entity->rss_elements[] = $media_arr;
  }

  return $element;
}
