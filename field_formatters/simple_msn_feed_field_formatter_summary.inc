<?php

function simple_msn_feed_rss_summary_formatterview($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  foreach ($items as $item) {
    // The formatter can be applied to text and long text fields only.
    if (!empty($item['value']) && in_array($field['type'], array('text', 'text_long'))) {
      $entity->rss_namespaces['xmlns:content'] = 'http://purl.org/rss/1.0/modules/content/';
      $entity->rss_elements[] = array(
        'key' => 'content:encoded',
        'value' => $item['safe_value'],
      );
    }
  }
  return $element;
}
