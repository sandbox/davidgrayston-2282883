<?php

/**
 * @file
 * Contains the admin form for the MSN feed.
 */

/**
 * Implements hook_admin_settings_form().
 */
function simple_msn_feed_admin_settings_form() {
  $form = array();

  $form['global_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('global configuration'),
    '#group' => 'vertical_tabs',
  );

  $form['global_config']['simple_msn_feed_image_style'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Image style'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_image_style'),
    '#options' => image_style_options(),
  );

  $form['global_config']['simple_msn_feed_publisher'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Publisher name'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_publisher'),
  );

  $form['global_config']['simple_msn_feed_credits'] = array(
    '#type' => 'textfield',
    '#title' => t('Default image Credits text'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_credits'),
  );

  $form['global_config']['simple_msn_feed_allowed_tags'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed tags'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_allowed_tags'),
  );

  $form['global_config']['simple_msn_feed_exclude_anchor_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude anchor links'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_exclude_anchor_links'),
  );

  $form['global_config']['simple_msn_feed_exclude_external_links'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude external links'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_exclude_external_links'),
  );

  $form['global_config']['simple_msn_feed_numeric_guid'] = array(
    '#type' => 'checkbox',
    '#title' => t('Rewrite GUID to become numeric (as node ID)'),
    '#default_value' => _simple_msn_feed_variable_get('simple_msn_feed_numeric_guid'),
  );

  return system_settings_form($form);
}
