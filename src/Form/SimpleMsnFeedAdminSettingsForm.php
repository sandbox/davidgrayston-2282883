<?php

/**
 * @file
 * Contains \Drupal\simple_msn_feed\Form\SimpleMsnFeedAdminSettingsForm.
 */

namespace Drupal\simple_msn_feed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the settings for simple msn feed.
 */
class SimpleMsnFeedAdminSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_msn_feed.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_msn_feed_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('simple_msn_feed.settings');

    $form['global_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('global configuration'),
      '#group' => 'vertical_tabs',
    );

    $form['global_config']['simple_msn_feed_image_style'] = array(
      '#type' => 'select',
      '#multiple' => FALSE,
      '#title' => t('Image style'),
      '#default_value' => $config->get('simple_msn_feed_image_style'),
      '#options' => image_style_options(),
    );

    $form['global_config']['simple_msn_feed_publisher'] = array(
      '#type' => 'textfield',
      '#title' => t('Default Publisher name'),
      '#default_value' => $config->get('simple_msn_feed_publisher'),
    );

    $form['global_config']['simple_msn_feed_credits'] = array(
      '#type' => 'textfield',
      '#title' => t('Default image Credits text'),
      '#default_value' => $config->get('simple_msn_feed_credits'),
    );

    $form['global_config']['simple_msn_feed_allowed_tags'] = array(
      '#type' => 'textfield',
      '#title' => t('Allowed tags'),
      '#default_value' => $config->get('simple_msn_feed_allowed_tags'),
    );

    $form['global_config']['simple_msn_feed_exclude_anchor_links'] = array(
      '#type' => 'checkbox',
      '#title' => t('Exclude anchor links'),
      '#default_value' => $config->get('simple_msn_feed_exclude_anchor_links'),
    );

    $form['global_config']['simple_msn_feed_exclude_external_links'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude external links'),
      '#default_value' => $config->get('simple_msn_feed_exclude_external_links'),
    );

    $form['global_config']['simple_msn_feed_numeric_guid'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Rewrite GUID to become numeric (as node ID)'),
      '#default_value' => $config->get('simple_msn_feed_numeric_guid'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
  }
}
